import React from 'react';
import './App.css';
import InputGroup from 'react-bootstrap/InputGroup';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl'
import Navbar from 'react-bootstrap/Navbar'

function App() {
  return (
    <div>
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand>Cadastro Cliente</Navbar.Brand>
      </Navbar>
      <p></p>
      <Form className="App">
        <InputGroup className="mb-3">
          <InputGroup.Prepend>
            <InputGroup.Text id="nome">Nome</InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            aria-label="Default"
            aria-describedby="nome"
          />
        </InputGroup>
        <InputGroup className="mb-3">
          <InputGroup.Prepend>
            <InputGroup.Text id="nome">CPF</InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            aria-label="Default"
            aria-describedby="nome"
          />
        </InputGroup>

        <InputGroup className="mb-3">
          <InputGroup.Prepend>
            <InputGroup.Text id="nome">CEP</InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            aria-label="Default"
            aria-describedby="nome"
          />
        </InputGroup>

      </Form>
    </div>

  );
}

export default App;
