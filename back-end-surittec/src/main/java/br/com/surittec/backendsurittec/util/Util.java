package br.com.surittec.backendsurittec.util;


public final class Util {
	
	/**
     * Remove a máscara de um texto.
     * @param texto Texto que deve ser removido a máscara
     * @return Retorna o texto sem máscara.
     */
    public static String removerMascara(String texto) {
        return texto != null ? texto.replaceAll("\\W", "") : texto;
    }

}
