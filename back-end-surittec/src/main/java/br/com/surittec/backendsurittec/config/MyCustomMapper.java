package br.com.surittec.backendsurittec.config;

import org.springframework.stereotype.Component;

import br.com.surittec.backendsurittec.domain.Cliente;
import br.com.surittec.backendsurittec.dto.ClienteDTO;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;

@Component
public class MyCustomMapper extends ConfigurableMapper {

    protected void configure(MapperFactory factory) {
        factory.classMap(Cliente.class, ClienteDTO.class)
        	.byDefault()
            .register();
    }
} 
