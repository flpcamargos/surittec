package br.com.surittec.backendsurittec.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import br.com.surittec.backendsurittec.enums.EnumTipoTelefone;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Telefone implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 443850089631664249L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private EnumTipoTelefone tipoTelefone;
	
	private String numero;

}
