package br.com.surittec.backendsurittec.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Endereco implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7687045924848720439L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String cep;

	private String logradouro;

	private String complemento;

	private String bairro;

	private String cidade;

	private String uf;
}
