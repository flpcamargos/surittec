package br.com.surittec.backendsurittec.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.surittec.backendsurittec.domain.Endereco;

public interface EnderecoRepository extends JpaRepository<Endereco, Long> {

}
