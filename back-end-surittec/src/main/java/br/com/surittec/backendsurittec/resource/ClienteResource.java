package br.com.surittec.backendsurittec.resource;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.surittec.backendsurittec.dto.ClienteDTO;
import br.com.surittec.backendsurittec.service.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteResource {

	private final ClienteService clienteService;

	public ClienteResource(ClienteService clienteService) {
		this.clienteService = clienteService;
	}

	@PostMapping("/novo")
	public ResponseEntity<ClienteDTO> createCliente(@RequestBody ClienteDTO clienteDTO) throws URISyntaxException {
		return new ResponseEntity<ClienteDTO>(clienteService.createCliente(clienteDTO), HttpStatus.OK);
	}
	
	@PutMapping("/editar")
	public ResponseEntity<ClienteDTO> editCliente(@RequestBody ClienteDTO clienteDTO) throws URISyntaxException {
		synchronized (this) {
			if(clienteDTO.getId() != null) {
				return new ResponseEntity<ClienteDTO>(clienteService.editCliente(clienteDTO), HttpStatus.OK); 
			}
			return new ResponseEntity<ClienteDTO>(HttpStatus.BAD_REQUEST);
        }
	}
	
	@GetMapping("/clientes")
	public ResponseEntity<List<ClienteDTO>> findAllClientes() throws URISyntaxException {
		
		return new ResponseEntity<List<ClienteDTO>>(clienteService.findAllClientes(), HttpStatus.OK); 
			
	}


}
