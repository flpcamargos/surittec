package br.com.surittec.backendsurittec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackEndSurittecApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackEndSurittecApplication.class, args);
	}

}
