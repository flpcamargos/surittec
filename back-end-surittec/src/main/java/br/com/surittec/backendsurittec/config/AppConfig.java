package br.com.surittec.backendsurittec.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages={"br.com.surittec.backendsurittec"})
public class AppConfig {
 
} 
