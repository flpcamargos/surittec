package br.com.surittec.backendsurittec.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmailClienteDTO {
	private Long id;

	private String email;
}
