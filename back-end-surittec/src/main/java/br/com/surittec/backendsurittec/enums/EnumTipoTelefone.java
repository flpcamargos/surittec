package br.com.surittec.backendsurittec.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EnumTipoTelefone {

	RESIDENCIAL(1, "Residencial"), COMERCIAL(2, "Comercial"), CELULAR(3, "Celular");
	
	private int cod;
	
	private String descricao;

}
