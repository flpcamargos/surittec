package br.com.surittec.backendsurittec.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClienteDTO {
	
	private Long id;

	private String nome;

	private String cpf;

	private EnderecoDTO endereco;

	private List<TelefoneDTO> telefones;

	private List<EmailClienteDTO> emails;
}
