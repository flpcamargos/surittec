package br.com.surittec.backendsurittec.dto;

import br.com.surittec.backendsurittec.enums.EnumTipoTelefone;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TelefoneDTO {
	private Long id;
	
	private EnumTipoTelefone tipoTelefone;
	
	private String numero;
}
