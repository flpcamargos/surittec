package br.com.surittec.backendsurittec.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.surittec.backendsurittec.domain.EmailCliente;

public interface EmailClienteRepository extends JpaRepository<EmailCliente, Long> {

}
