package br.com.surittec.backendsurittec.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.surittec.backendsurittec.domain.Telefone;

public interface TelefoneRepository extends JpaRepository<Telefone, Long> {

}
