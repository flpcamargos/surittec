package br.com.surittec.backendsurittec.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.surittec.backendsurittec.domain.Cliente;
import br.com.surittec.backendsurittec.dto.ClienteDTO;
import br.com.surittec.backendsurittec.repository.ClienteRepository;
import br.com.surittec.backendsurittec.util.Util;
import ma.glasnost.orika.MapperFacade;

@Service
@Transactional
public class ClienteService {

	private final ClienteRepository clienteRepository;
	
	@Autowired
    MapperFacade mapper;

	public ClienteService(ClienteRepository clienteRepository) {

		this.clienteRepository = clienteRepository;
	}

	public ClienteDTO createCliente(ClienteDTO clienteDTO) {
		
		clienteDTO.setCpf(Util.removerMascara(clienteDTO.getCpf()));
		clienteDTO.getEndereco().setCep(Util.removerMascara(clienteDTO.getEndereco().getCep()));
		
		Cliente cliente = mapper.map(clienteDTO, Cliente.class);

		clienteRepository.save(cliente);

		return mapper.map(cliente, ClienteDTO.class);

	}

	public ClienteDTO editCliente(ClienteDTO clienteDTO) {
		Cliente cliente = mapper.map(clienteDTO, Cliente.class);

		clienteRepository.save(cliente);

		return mapper.map(cliente, ClienteDTO.class);
	}

	public List<ClienteDTO> findAllClientes() {
		return mapper.mapAsList(clienteRepository.findAll(), ClienteDTO.class);
	}

}
